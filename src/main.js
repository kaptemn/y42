import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import './assets/scss/main.scss';

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faUserSecret, faArrowUp, faArrowRight } from '@fortawesome/free-solid-svg-icons'

library.add(faUserSecret, faArrowUp, faArrowRight)


const app = createApp(App);

app.use(router)
app.component('font-awesome-icon', FontAwesomeIcon)


app.mount('#app');
