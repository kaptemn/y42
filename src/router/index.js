import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },   

  ]
})

// router.beforeResolve( (to, from, next) => {
//   if (to.meta.isAuthenticated) {

//     const authStore = useAuthStore();
    
//     if (authStore.loggedIn && authStore.token) {
//       return next();
//     }

//     if (authStore.token) {
//       return authStore.getActiveCustomer().then(() => {
//         next();
//       })
//     }

//     return next('/');
//   }

//   return next();
// })

export default router
