# Vue3 - Y42

## Demo 

Figma:
[figma design](https://www.figma.com/file/g1NCbfyYaVcgWF0c8eItLQ/y42-my?node-id=0-1&t=wWEZCGDaWyG9fuGy-0)

Here is a live demo page and the repo for it:
[https://fir-project-y42.web.app/](https://fir-project-y42.web.app/)

## Setup

Make sure to install the dependencies:

```bash
# npm
npm install
```

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

